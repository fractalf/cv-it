const data = {
    profile: {
        name: 'Alf Marius Foss Olsen',
        phone: '+47 90027865',
        email: 'post@alfmarius.net',
        // address: 'Rødstuveien 4, 0572 Oslo',
        birthdate: '27.11.1974',
        sivil: 'Samboer og datter på 1 år',
        image: 'images/alf.png',
        about: 'Jeg er en imøtekommende og blid person med stor interesse for teknologi knyttet til verdensveven.\
                Daglig holder jeg meg oppdatert på hva som skjer innenfor IT. Plukker raskt opp nye ting og liker utfordringer\
                og spennede prosjekter.\
                Om noen kaller meg geek takker jeg for komplimentet og smiler stolt for meg selv.'

    },
    sections: {
        education: {
            title: 'Utdanning',
            items: [
                {
                    title: 'Universitetet i Oslo',
                    subTitle: 'Cand.mag (bachlor) Informatikk',
                    date: '1995 - 1998<br>1999 - 2001',
                    p: [
                        'Informatikk, matte, fysikk, kjemi, astronomi'
                    ]
                },
                {
                    title: 'Vestoppland folkehøyskole',
                    subTitle: 'Samspill og konfliktløsning',
                    date: '1998 - 1999',
                    p: [
                        'Prosesser rundt konflikter og hvordan løse dem'
                    ]
                },
                {
                    title: 'St. Svithun videregående skole',
                    subTitle: 'Generell studiekompetanse',
                    date: '1990 - 1992<br>1993 - 1994',
                    p: [
                        'Almenne fag, matte, fysikk, kjemi, tysk'
                    ]
                },
                {
                    title: 'Nooksack Valley High School',
                    subTitle: 'Utvekslingselev i USA',
                    date: '1992 - 1993',
                    p: [
                        'Generelle fag, engelsk, matte, fysikk, sport, håndverk'
                    ]
                }
            ]
        },
        relevantExperience: {
            title: 'Relevant erfaring',
            items: [
                {
                    title: 'Prisguide AS',
                    subTitle: 'Utviklingssjef',
                    date: '2014 - 2017+',
                    p: [
                        'Prisguide tilbyr produktinformasjon, prissammenligning og andre nyttige forbrukertjenester knyttet til netthandel\
                        via nettsiden <a href="https://www.prisguide.no">prisguide.no</a>. De samarbeider med flere hundre nettbutikker\
                        og andre partnere med å sanke inn og levere store mengder data om alle mulige typer produkter som kan\
                        kjøpes på nett.'
                    ],
                    points: [
                        'Ansvar for andre utviklere og den tekniske plattformen',
                        'PHP, CSS, HTML og Javascript utvikling',
                        'Lagre data i MySQL/PostgreSQL database',
                        'Indexere data i Apache Solr for søketjenester',
                        'Crawle nettsider med egenutviklet Node.js programvare',
                        'Utvikle REST API for partnere',
                        'Lage enkle eksport og widget løsninger',
                        'git for best mulig kode-flyt blandt utviklere',
                        'GNU/Linux som hovedplattform'
                    ]
                },
                {
                    title: 'Mediehuset Tek AS',
                    subTitle: 'Programmerer',
                    date: '2012 - 2014',
                    p: [
                        'Tek har flere kjente tekniske nettsider som <a href="https://www.tek.no">tek.no</a>\
                         (tidligere hardware.no, akam.no, amobil.no), diskusjon.no, gamer.no og insidetelecom.no.\
                         De startet også tjenesten prisguide.no som til slutt ble et eget aksje.'
                    ],
                     points: [
                        'Integrere løsninger på tvers av nettsidene',
                        'Videreutvikle tjenesten prisguide.no',
                        'PHP, CSS, HTML, Javascript, MySQL, Solr, git',
                        'GNU/Linux som hovedplattform'
                    ]
                },
                {
                    title: 'SnowCastle AS',
                    subTitle: 'Programmerer og driftsansvarlig',
                    date: '2009 - 2012',
                    p: [
                        '<a href="https://www.snowcastlegames.com/">SnowCastle</a> er et spillselskap\
                         som utvikler spill og apps til alle plattformer. De har blant annet levert til partnere som\
                         TV2, Forsvaret, Hurtigruten og flere andre. Seneste årene har alt fokus vært på storsatsingen\
                         <a href="https://www.snowcastlegames.com/earthlock/">EarthLock - Festival of Magic</a>.'
                    ],
                    points: [
                        'Større spill/apps for alle plattformer (Unity 3D, C#, Javascript)',
                        'Mindre spill/apps for web, facebook, o.l. (HTML5, Flash, AS3)',
                        'Utvikling og drift av backend-løsninger (Linux, PHP, MySQL)'
                    ]
                },
                {
                    title: 'Game Index AS',
                    subTitle: 'Programmerer og driftsansvarlig',
                    date: '2006 - 2009',
                    p: [
                        'Game Index (aka <i>gameXplore</i>) samlet inn og kategoriserte objektiv\
                         analyse/informasjon om dataspill. Dataene ble levert til kunder både\
                          fysisk og elektronisk samt utgitt i to bøker. Selskapet ble lagt ned til\
                           fordel for SnowCastle AS for videre satsing på spillutvikling.'
                    ],
                    points: [
                        'Utvikling og drift av web-baserte verktøy (HTML5, PHP, MySQL)',
                        'Kundesupport'
                    ]
                },
                {
                    title: 'USIT (Universitetet i Oslo)',
                    subTitle: 'Avdelingsingeniør',
                    date: '1997 - 2002',
                    p: [
                        '<a href="http://www.usit.uio.no/">USIT</a> har ansvaret for all drift\
                         og utvikling ved universitetets fakulteter og institutt.'
                    ],
                    points: [
                        'Drift av UiO sine kurssenter',
                        'Løsninger for distribuering av programvare',
                        'Automatisering og scripting (Perl)',
                        'Backupløsninger',
                        'Generell drift av ansattes PCer'
                    ]
                }
            ]
        },
        otherExperience: {
            title: 'Annen erfaring',
            items: [
                {
                    title: 'Rustadgrenda omsorgsbolig',
                    subTitle: 'Miljøarbeider',
                    date: '2005 - 2006'
                },
                {
                    title: 'Langleiken barnehage',
                    subTitle: 'Barnehagevikar',
                    date: '2004'
                },
                {
                    title: 'Friluftssenteret i Gamle Oslo',
                    subTitle: 'Siviltjeneste',
                    date: '2002-2003'
                },
                {
                    title: 'Krishnas Cuisine',
                    subTitle: 'Assisterende kokk og servitør',
                    date: '2001 - 2006'
                },
                {
                    title: 'Livets reiseskole',
                    date: '1999 - 2005',
                    p: [
                        'Praktisk livserfaring som backpacker i India, Sør-Afrika, Brasil, Thailand, Japan og Sør-Europa'
                    ]
                },

            ]
        },
        skills: {
            title: 'Ferdigheter',
            items: [
                {
                    title: 'Programmeringsspråk',
                    p: [
                        'Har over 20 år med programmeringerfaring. Ekstra god erfaring med <b>PHP</b> og <b>Javascript (ES6+)</b>, men\
                        har god kjennskap til andre språk som <b>SQL</b>, <b>Java</b>, <b>Bash</b>, <b>C#</b>, <b>C++</b>, <b>C</b>, <b>Go</b>, <b>Perl</b> og flere.',
                        '<i>"Kan man programmere er det enkelt å lære seg et nytt språk</i>"',
                    ],
                },
                {
                    title: 'OS og programvare',
                    p: [
                        'Har tilbrakt mye tid foran skjermen siden barndommen, så all bruk av programmvare faller meg veldig naturlig.\
                        Windows og Linux er de OSene jeg har brukt mest.',
                        'I jobbsammenheng har jeg god kjennskap til jQuery, Vue.js, Node.js, Docker, Nginx,\
                        Elastic, Solr, Ansible, Atom, Sublime, Git, Firefox, Vivaldi, Unity 3D, Photoshop, Ableton.',
                        'Utenom disse har jeg naturligvis brukt utallige andre teknologier og programvare.'
                    ]
                }
            ]
        },
        portfolio: {
            title: 'Portfolio',
            items: [
                {
                    p: [
                        'Her er noen forskjellige ting jeg har laget',
                    ],
                    points: [
                        'CV - denne cv-en er laget med Vue.js (<a href="https://gitlab.com/fractalf/cv-it">kildekode</a>)',
                        'Edderkopp - crawler laget med node.js (<a href="https://github.com/fractalf/edderkopp">kildekode</a>)',
                        'JS1K - Javascript konkurranse på maks 1024 bytes (<a href="http://js1k.com/2016-elemental/demo/2603">2016</a>,\
                        <a href="http://js1k.com/2017-magic/demo/2727">2017</a>)',
                        'Ereg - webside for registrering til Kollensvevet/Yourway AS (<a href="images/kollensvevet1.png">forside</a>, \
                        <a href="images/kollensvevet2.png">adminside</a>)',
                        'Bondesjakk - facebookspill laget til Grønn Utdanning (<a href="images/bondesjakk1.png">bilde 1</a>, \
                        <a href="images/bondesjakk2.png">bilde 2</a>)',
                        'Hogworld: Gnart\'s Adventure - interaktiv lydbok (<a href="images/gnart.png">bilde</a>, \
                        <a href="https://www.snowcastlegames.com/hogworld/">link</a>, \
                        <a href="https://www.wired.com/2011/12/hogworld-gnarts-adventure/">review</a>)',
                    ]
                }
            ]
        },
        interests: {
            title: 'Interesser',
            items: [
                {
                    p: [
                        'Programmering (scene), sjakk, fraktalteori og -kunst, innebandy, ølbrygging,\
                        slalom skateboard, downhill longboard, snowboard, musikkproduksjon',
                    ]
                }
            ]
        },
        references: {
            title: 'Referanser',
            items: [
                {
                    // title: 'Interesser',
                    points: [
                        'Amund Espelien (amund@prisguide.no, 97072033), daglig leder Prisguide AS',
                        'Bendik Stang (bendik@snowcastle.no, 99275829), daglig leder SnowCastle AS',
                    ]
                },
            ]
        }
    }
}


export const profile = data.profile;
export const sections = data.sections;
